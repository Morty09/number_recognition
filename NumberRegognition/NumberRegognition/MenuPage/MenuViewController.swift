//
//  MenuViewController.swift
//  NumberRegognition
//
//  Created by Bérangère La Touche on 11/12/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit
import Contacts

class MenuViewController: UIViewController {

    @IBOutlet weak var trainItem: UIBarButtonItem!
    @IBOutlet weak var callItem: UIBarButtonItem!
    @IBOutlet weak var titleView: UINavigationItem!
    @IBOutlet weak var contactsTableView: UITableView!
    
    let contactStore = CNContactStore()
    var contacts : [CNContact] = [] {
        didSet {
            self.contactsTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleView.title = "Mes contacts"
        self.trainItem.width = self.view.bounds.width / 2
        self.callItem.width = self.view.bounds.width / 2
        self.contactsTableView.delegate = self
        self.contactsTableView.dataSource = self
        
        fetchContacts()
    
    }
    
    func fetchContacts() {
        contactStore.requestAccess(for: .contacts) { (res, err) in
            if (err != nil) {
                print("Failed to request access | Reason : \(err.debugDescription)")
            }
            
            if (res) {
                
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                do {
                    
                    try self.contactStore.enumerateContacts(with: request, usingBlock: { (contact, err) in
                        DispatchQueue.main.async {
                            if (self.contacts.count <= 9) {
                                self.contacts.append(contact)
                            }
                        }
                    })
                    
                } catch let err {
                    print("Failed to enumerate contacts : ", err)
                }
                
            } else {
                print("Access Denied, allow authorization")
            }
        }
    }
    
    @IBAction func touchItem(_ sender: UIBarButtonItem) {
        if (sender.tag == 0) {
            let drawViewTrain = DrawViewTrainController()
            self.navigationController?.pushViewController(drawViewTrain, animated: false)
        } else if (sender.tag == 1) {
            let drawViewPredict = DrawViewPredictController()
            drawViewPredict.contacts = self.contacts
            self.navigationController?.pushViewController(drawViewPredict, animated: false)
        }
    }
}

extension MenuViewController : UITableViewDelegate {
    
}

extension MenuViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = ("\(indexPath.row). \(contacts[indexPath.row].familyName) \(contacts[indexPath.row].givenName)")
        
        return cell
    }
    
    
}
