//
//  APICalls.swift
//  NumberRegognition
//
//  Created by Bérangère La Touche on 11/12/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit

class APICalls {
    
    let server : String = "51.75.254.220"
    let local : String = "192.168.1.13"
    
    func getTag(completion: @escaping (_ result: [String : Any]?)->()) {
        
        //51.75.254.220
        guard let url = URL(string: "http://\(self.server):3000/tag") else {
            return
        }
        
        print(url)
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: {(data, res, err) in
            
            guard
                let d = data,
                let obj = try? JSONSerialization.jsonObject(with: d, options: .allowFragments),
                let p = obj as? [String : Any] else {
                    return
            }
            
            DispatchQueue.main.async {
                //print(p)
                completion(p)
            }
        })
        task.resume()
    }
    
    func uploadImageOnServer(number: Int, image: UIImage, completion: @escaping (_ result: [String : Any]?)->()) {
        
        //51.75.254.220
        guard let url = URL(string: "http://\(self.server):3000/dataset/\(String(describing: number))") else {
            return
        }
        
        print(url)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        /*let param : [String : Any] = [
         "face" : "berangere"
         ]*/
        
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let imageData = image.jpegData(compressionQuality: 1.0) else {
            return
        }
        
        let body = createBodyWithParameters(filePathKey: "image", imageDataKey: imageData, boundary: boundary)
        
        print(body)
        
        //request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "image", imageDataKey: imageData, boundary: boundary)
        //let jsonData = try? JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
        //print(jsonData!)
        request.httpBody = body
        
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            
            // You can print out response object
            print("******* response = \(String(describing: response))")
            
            // Print out reponse body
            let responseString = String(data: data, encoding: String.Encoding.utf8)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                
                print(json as Any)
                
                DispatchQueue.main.async {
                    //self.myActivityIndicator.stopAnimating()
                    //self.myImageView.image = nil;
                }
                
            }catch
            {
                print(error)
            }
            
        }
        
        task.resume()
        
    }
    
    func train(completion: @escaping (_ result: [String : Any]?)->()) {
        
        //51.75.254.220
        guard let url = URL(string: "http://\(self.server):3000/train") else {
            return
        }
        
        print(url)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            
            // You can print out response object
            print("******* response = \(String(describing: response))")
            
            // Print out reponse body
            let responseString = String(data: data, encoding: String.Encoding.utf8)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                
                print(json as Any)
                completion((json as! [String : Any]))
                
                DispatchQueue.main.async {
                }
                
            }catch
            {
                print(error)
            }
            
        }
        
        task.resume()
        
    }
    
    func predictImage(image: UIImage, completion: @escaping (_ result: String?)->()) {
        
        //51.75.254.220
        guard let url = URL(string: "http://\(self.server):3000/predict") else {
            return
        }
        
        print(url)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        /*let param : [String : Any] = [
         "face" : "berangere"
         ]*/
        
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let imageData = image.jpegData(compressionQuality: 1.0) else {
            return
        }
        
        let body = createBodyWithParameters(filePathKey: "image", imageDataKey: imageData, boundary: boundary)
        
        print(body)
        
        //request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "image", imageDataKey: imageData, boundary: boundary)
        //let jsonData = try? JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
        //print(jsonData!)
        request.httpBody = body
        
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            
            // You can print out response object
            print("******* response = \(String(describing: response))")
            
            // Print out reponse body
            let responseString = String(data: data, encoding: String.Encoding.utf8)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                
                print(json as Any)
//                let stateCode = json?["data"] as? Int ?? Int(json?["data"] as? String ?? "")
                let stateCode = json?["data"] as? [Any]
                //print(stateCode![0])
                if let value = (stateCode![0] as? String) {
                    print(value)
                    completion(value)
                }
                //completion(stateCode![0])
                
                DispatchQueue.main.async {
                    //self.myActivityIndicator.stopAnimating()
                    //self.myImageView.image = nil;
                }
                
            }catch
            {
                print(error)
            }
            
        }
        
        task.resume()
        
    }
    
    //func createBodyWithParameters(parameters: [String: Any]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
    func createBodyWithParameters(filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        var body = Data()
        
        /*if parameters != nil {
         for (key, value) in parameters! {
         body.appendString("--\(boundary)\r\n")
         body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
         body.appendString("\(value)\r\n")
         }
         }*/
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString("\r\n")
        
        
        
        body.appendString("--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
}

extension Data {
    
    mutating func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
