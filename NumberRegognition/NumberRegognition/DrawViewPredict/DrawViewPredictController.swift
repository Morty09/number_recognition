//
//  DrawViewPredictController.swift
//  NumberRegognition
//
//  Created by Bérangère La Touche on 11/12/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit
import SwiftSignatureView
import Contacts

class DrawViewPredictController: UIViewController {

    @IBOutlet weak var drawableView: SwiftSignatureView!
    @IBOutlet weak var predictionLabel: UILabel!
    
    var img = UIImage()
    var api = APICalls()
    var currentValueAsked : Int!
    var contacts : [CNContact]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.drawableView.delegate = self
    }
    
    @IBAction func touchSendButton(_ sender: Any) {
        img = drawableView.signature!
        
        //let myThumb1 = img.resized(withPercentage: 0.1)
        img = img.rezisedImage(image: img, scale: 3)
        let myThumb2 = img.resized(toWidth: 45.0)
        
        api.predictImage(image: myThumb2!) { (res) in
            DispatchQueue.main.async {
                self.predictionLabel.text = res ?? "No value"
                self.getContactAssociated(value: Int(res!)!)
            }
        }
    }
    
    func getContactAssociated(value : Int) {
        print(value)
        var isExist : Bool = false
        for (index, contact) in self.contacts.enumerated() {
            if (value == index) {
                alertForCall(contact: contact)
                isExist = true
            }
        }
        
        if (!isExist) {
            alertForNoContact()
        }
    }
    
    func alertForCall(contact : CNContact) {
        let alertController = UIAlertController(title: "Contact", message: "Voulez-vous appeler le contact: \(contact.familyName) \(contact.givenName)", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Non", style: .cancel))
        alertController.addAction(UIAlertAction(title: "Oui", style: .default) { (result) in
            self.callContact(contact: contact)
        })
        self.present(alertController, animated: true)
    }
    
    func alertForNoContact() {
        let alertController = UIAlertController(title: "Contact", message: "Aucun contact à ce numéro, réessayez !", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .cancel))
        self.present(alertController, animated: true)
    }
    
    func callContact(contact: CNContact) {
        if let call = contact.phoneNumbers.first?.value.stringValue,
            let url = URL(string: "tel://\(call)"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func touchClearButton(_ sender: Any) {
        drawableView.clear()
    }
    
    @IBAction func touchBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}

extension DrawViewPredictController : SwiftSignatureViewDelegate {
    
    public func swiftSignatureViewDidTapInside(_ view: SwiftSignatureView) {
        //print("Did tap inside")
    }
    
    public func swiftSignatureViewDidPanInside(_ view: SwiftSignatureView) {
        //print("Did pan inside")
    }
    
}

extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func rezisedImage(image: UIImage, scale: CGFloat) -> UIImage {
        let newHeight = image.size.height / scale
        let newWidth = image.size.width / scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
