//
//  DrawViewController.swift
//  NumberRegognition
//
//  Created by Bérangère La Touche on 05/12/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit
import SwiftSignatureView

class DrawViewTrainController: UIViewController {

    @IBOutlet weak var drawableView: SwiftSignatureView!
    @IBOutlet weak var trainNumberLabel: UILabel!
    
    var img = UIImage()
    var api = APICalls()
    var currentValueAsked : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.drawableView.delegate = self
        
        getNumbersFromServer()
    }
    
    func getNumbersFromServer() {
        api.getTag { (res) in
            let stateCode = res?["data"] as? Int ?? Int(res?["data"] as? String ?? "")
            print(stateCode!)
            self.currentValueAsked = stateCode
            self.trainNumberLabel.text = res?["data"] as? String ?? "No value"
        }
    }
    
    func upload() {
        api.uploadImageOnServer(number: self.currentValueAsked, image: self.img) { (res) in
            print(res!)
        }
    }
    
    func train() {
        api.train { (res) in
            print("Train response : \(res!)")
            let alertController = UIAlertController(title: "Train OK", message: "L'entrainement a réussi !", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .cancel))
            DispatchQueue.main.async {
                self.present(alertController, animated: true)
            }
        }
    }
    
    @IBAction func touchSendButton(_ sender: Any) {
        img = drawableView.signature!
        img = img.rezisedImage(image: img, scale: 3)
        img = img.resized(toWidth: 45.0)!
        
        // scale =1 et sans resize => w: 343, h: 483
        // scale = 3 avec rezise a 45 => w:45, h: 63
        
        self.drawableView.clear()
        upload()
        getNumbersFromServer()
    }
    
    @IBAction func touchClearButton(_ sender: Any) {
        drawableView.clear()
    }
    
    @IBAction func touchBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func touchTrainOverButton(_ sender: Any) {
        train()
    }
}

extension DrawViewTrainController : SwiftSignatureViewDelegate {
    
    public func swiftSignatureViewDidTapInside(_ view: SwiftSignatureView) {
        //print("Did tap inside")
    }
    
    public func swiftSignatureViewDidPanInside(_ view: SwiftSignatureView) {
        //print("Did pan inside")
    }
    
}
