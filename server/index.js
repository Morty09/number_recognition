const express = require('express');
const multer = require('multer');
const fs = require('fs');
const spawn = require('child_process').spawn;

const DATASET_DIRECTORY = './dataset/';
const TMP_DIRECTORY = './tmp/';
const TMP_FILE_NAME = 'image__.png';

let result = [];
let fileNumer = wichNumber();

function wichNumber() {
    let less = {
        dir: "0",
        len: 80000
    };

    fs.readdirSync(DATASET_DIRECTORY).forEach(file => {
        let imageDir = DATASET_DIRECTORY + file
        let len = fs.readdirSync(imageDir).length;
        if (len < 45000 && len < less.len) {
            less = {
                dir: file,
                len: len
            };
        }
    });
    return less.dir;
}

let tmpStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, TMP_DIRECTORY);
    },
    filename: function (req, file, cb) {
        cb(null, TMP_FILE_NAME);
    }
});

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        let fullpath = DATASET_DIRECTORY + req.params.number;
        cb(null, fullpath)
    },
    filename: function (req, file, cb) {
        let imageDir = DATASET_DIRECTORY + req.params.number;
        fs.readdir(imageDir, (err, files) => {
            let index;
            if (files.length == 0) {
                index = "00002";
            } else {
                let nameLastFile = files[files.length - 1]
                if (nameLastFile == ".DS_Store") {
                    index = "00002";
                } else {
                    index = nameLastFile.substring(2, nameLastFile.indexOf("."));
                    index = parseInt(index) + 1
                    if (index < 10) {
                        index = "0000" + index
                    } else if (index < 100) {
                        index = "000" + index
                    } else if (index < 1000) {
                        index = "00" + index
                    } else if (index < 10000) {
                        index = "0" + index
                    }
                }
            }
            cb(null, req.params.number + '_' + index + '.png');
        });
    }
});

const upload = multer({ storage: storage });
const tmpUpload = multer({ storage: tmpStorage });
let app = express();

// CREATE DATASET
app.post('/dataset/:number', upload.single('image'), function (req, res, next) {
    console.log(">> image upload");
    res.status(200).send({ data: 'Upload ok' });
});

// TRAIN
app.post('/train/', function (req, res, next) {
    console.log('>> train in progress ...');

    let process = spawn('python3', ["../NumberRecognitionPython/NRPy/Train_v2.py", req.file]);

    process.on('exit', function (data) {
        console.log('<< python end:', data);
        if (data != 0) {
            res.status(200).send({ data: "Error in python program, please look logs" });
            return;
        }
        res.status(200).send({ data: 'Train ok' });
    });

    process.stdout.on('data', function (data) {
        console.log('<< python debug:', data.toString('utf8'));
    });

    process.stderr.on('data', function (data) {
        console.log('<< python error:', data.toString('utf8'));
    });
});

// PREDICT
app.post('/predict/', tmpUpload.single('image'), function (req, res, next) {
    result = [];
    console.log(">> predict in progress ...");

    let process = spawn('python3', ["../NumberRecognitionPython/NRPy/Predict.py", req.file]);

    process.on('exit', function (data) {
        console.log('<< python end:', data);
        //fs.unlinkSync(TMP_DIRECTORY + TMP_FILE_NAME); // supprime le fichier temporaire
        if (data != 0) {
            res.status(200).send({ data: "Error in python program, please look logs" });
            return;
        }
        res.status(200).send({ data: result });
    });

    process.stdout.on('data', function (data) {
        if (data.includes('$')) {
            tmp = "" + data;
            tmp = tmp.substr(0,2);
            result.push(tmp[1])
        }
        console.log('<< python debug:', data.toString('utf8'));
    });

    process.stderr.on('data', function (data) {
        console.log('<< python error:', data.toString('utf8'));
    });
});

// GETTAG
app.get('/tag/', function (req, res, next) {

    console.log('>>', fileNumer);
    let number = fileNumer;
    let num = parseInt(fileNumer) + 1;

    if (num > 9)
       num = 0;
    
    fileNumer = '' + num;
    console.log('>> ', fileNumer);
    res.status(200).send({ data: number });
});

console.log(`>> Server started and listening on port 3000`);
app.listen(3000);
