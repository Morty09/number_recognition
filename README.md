# Number Recognition

This application developped in Swift 4 is a number regognition app.
You design a number on the screen and the machine automatically recognize your number.
It is linked with a python scrypt through a NodeJS server who is the "machine learning" and do the recognition and the learning.

* Swift 4 application
* Machine learning
* NodeJS Server
* Python scrypt

## Purpose

Project realized in the context of school.

The aim of the project was to learn the machine learning and create an application who included it.

