import numpy as np
import os
import sys
import pickle

from PIL import Image, ImageFilter
from sklearn import tree
from joblib import dump, load

print(os.getcwd())

directoryPath = "./dataset"
imgToFind = "./tmp/image__.png"

def main():
    print("Train.py - START")
    sys.stdout.flush()
    createdataset()


def createdataset():
    data_x = []
    label_y = []

    for filename in os.listdir(directoryPath):
        if filename != ".DS_Store":
            for img in os.listdir(directoryPath + "/" + filename):
                if img != ".DS_Store":
                    label_y.append(filename)
                    im = Image.open(directoryPath + "/" + filename + "/" + img)
                    im = im.filter(ImageFilter.BLUR)
                    im = im.filter(ImageFilter.BLUR)
                    im = im.filter(ImageFilter.BLUR)
                    im = im.filter(ImageFilter.BLUR)
                    im = im.filter(ImageFilter.BLUR)
                    im = im.convert('I')
                    pix = np.array(im)
                    pix = pix.reshape((im.size[0] * im.size[1],))
                    data_x.append(pix)

    x_train = np.array(data_x, object)
    y_train = np.array(label_y, object)
    x_train.shape, y_train.shape

    print("Train...")
    sys.stdout.flush()

    tree_clf = tree.DecisionTreeClassifier().fit(x_train, y_train)
    pickle.dump(tree_clf, open('tree_clf_dump.joblib', "wb"))

    print("TRAIN OVER")
    sys.stdout.flush()
    exit(0)


if __name__ == "__main__":
    main()
