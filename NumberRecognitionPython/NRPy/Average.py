import numpy as np
import os
import sys
import pickle

from PIL import Image, ImageFilter
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import train_test_split
from sklearn import tree, svm
from joblib import dump, load

print(os.getcwd())

#directoryPath = "./dataset"
directoryPath = "../../server/dataset"

def main():
    print("Stat classifier - START")
    sys.stdout.flush()
    createdataset()


def createdataset():
    data_x = []
    label_y = []

    for filename in os.listdir(directoryPath):
        if filename != ".DS_Store":
            for img in os.listdir(directoryPath + "/" + filename):
                if img != ".DS_Store":
                    label_y.append(filename)
                    im = Image.open(directoryPath + "/" + filename + "/" + img)
                    im = im.filter(ImageFilter.BLUR)
                    im = im.filter(ImageFilter.BLUR)
                    im = im.filter(ImageFilter.BLUR)
                    im = im.filter(ImageFilter.BLUR)
                    im = im.filter(ImageFilter.BLUR)
                    im = im.convert('I')
                    pix = np.array(im)
                    pix = pix.reshape((im.size[0] * im.size[1],))
                    data_x.append(pix)

    x_train = np.array(data_x, object)
    y_train = np.array(label_y, object)
    x_train.shape, y_train.shape

    sgd_res_stat = list()
    tree_res_stat = list()
    svc_res_stat = list()

    for count in range(0, 20):
        X_train, X_test, Y_train, Y_test = train_test_split(x_train, y_train, test_size=0.2, shuffle=True, random_state=20)

        sgd_clf = SGDClassifier(random_state=40, max_iter=1000, tol=1e-3).fit(X_train, Y_train)
        sgd_res = sgd_clf.predict(X_test)
        result = (sum(sgd_res == Y_test))/len(Y_test)
        sgd_res_stat.append(result)

        tree_clf = tree.DecisionTreeClassifier().fit(X_train, Y_train)
        tree_res = tree_clf.predict(X_test)
        result = (sum(tree_res == Y_test))/len(Y_test)
        tree_res_stat.append(result)

        svc_clf = svm.SVC(gamma=0.001).fit(X_train, Y_train)
        svc_res = svc_clf.predict(X_test)
        result = (sum(svc_res == Y_test))/len(Y_test)
        svc_res_stat.append(result)

    print("write data")
    file = open("output_stat.txt","w")

    for val in sgd_res_stat:
        file.write("%.3f;" % val)

    file.write('\n\r')

    for val in tree_res_stat:
        file.write("%.3f;" % val)

    file.write('\n\r')

    for val in svc_res_stat:
        file.write("%.3f;" % val)

    file.write('\n\r')
    file.close()

    print("Stat classifier OVER")
    sys.stdout.flush()
    exit(0)


if __name__ == "__main__":
    main()
