import numpy as np
import os
import sys
import pickle

from PIL import Image, ImageFilter
from sklearn import tree
from joblib import dump, load

print(os.getcwd())

directoryPath = "./dataset"
imgToFind = "./tmp/image__.png"


def main():
    print("Predict.py - START")
    sys.stdout.flush()
    predictNumber()


def predictNumber():

    img_test = Image.open(imgToFind)
    img_test = img_test.filter(ImageFilter.BLUR)
    img_test = img_test.filter(ImageFilter.BLUR)
    img_test = img_test.filter(ImageFilter.BLUR)
    img_test = img_test.filter(ImageFilter.BLUR)
    img_test = img_test.filter(ImageFilter.BLUR)
    img_test = img_test.convert('I')
    np_img_test = np.array(img_test)
    np_img_test = np_img_test.reshape((img_test.size[0] * img_test.size[1],))
    x_test = np.array(np_img_test)

    tree_clf = load('tree_clf_dump.joblib')
    tree_res = tree_clf.predict([x_test])

    print("$" + tree_res[0])
    sys.stdout.flush()

    print("PREDICT OVER")
    sys.stdout.flush()
    exit(0)


if __name__ == "__main__":
    main()
